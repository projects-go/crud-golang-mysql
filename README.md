# crud-golang-mysql

Implementación de operaciones básicas de persistencia en una base de datos, las cuales generalente son referidas por su acrónimo CRUD (Create, Read, Update and Delete). La interacción se lleva a cabo por medio de la consola

## Conceptos a desarrollar

CRUD

## Tecnologías utilizadas

Lenguaje de programación: Golang

Gestor de base de datos: MySQL

## Uso

Para iniciar el proyecto ejecutar el siguiente comando:

```bash
1) go run *.go
```

## Referencias
[parzibyte](https://parzibyte.me/blog/2018/12/10/crud-golang-mysql/)
