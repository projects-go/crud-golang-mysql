package main

func read() ([]Contact, error) {
	contacts := []Contact{}
	db, err := dbConn()
	if err != nil {
		return nil, err
	}
	defer db.Close()
	
	registers, err := db.Query("SELECT id, name, address FROM contacts")
	if err != nil {
		return nil, err
	}
	defer registers.Close()

	var c Contact
	for registers.Next() {
		err := registers.Scan(&c.Id, &c.Name, &c.Address)
		if err != nil {
			return nil, err
		}
		contacts = append(contacts, c)
	}
	return contacts, nil
}