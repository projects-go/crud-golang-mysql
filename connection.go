package main

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

func dbConn() (db *sql.DB, e error) {
	dbDriver := "mysql"
	dbUser := "root"
	dbPass := "almirante" // INGRESAR PASSWORD
	dbHost := "tcp(127.0.0.1:3306)"
	dbName := "crud"
	db, err := sql.Open(dbDriver, dbUser + ":" + dbPass + "@" + dbHost + "/" + dbName)
	if err != nil {
		return nil, err
	}
	return db, nil
}
