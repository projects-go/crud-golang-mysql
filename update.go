package main

func update(c Contact) error {
	db, err := dbConn()
	if err != nil {
		return err
	}
	defer db.Close()

	preparedStatement, err := db.Prepare("UPDATE contacts SET name = ?, address = ? where id = ?")
	if err != nil {
		return err
	}
	defer preparedStatement.Close()
	_, err = preparedStatement.Exec(c.Name, c.Address, c.Id)
	// Ya sea nil o sea un error, lo manejaremos desde donde hacemos la llamada
	return err
}