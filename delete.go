package main

func delete(c Contact) error {
	db, err := dbConn()
	if err != nil {
		return err
	}
	defer db.Close()

	preparedStatement, err := db.Prepare("DELETE FROM contacts WHERE id = ?")
	if err != nil {
		return err
	}
	defer preparedStatement.Close()

	_, err = preparedStatement.Exec(c.Id)
	if err != nil {
		return err
	}
	return nil
}