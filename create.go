package main

func create(c Contact) (e error) {
	db, err := dbConn()
	if err != nil {
		return err
	}
	defer db.Close()

	// Para prevenir inyecciones SQL
	preparedStatement, err := db.Prepare("INSERT INTO contacts (name, address) values (?,?)")
	if err != nil {
		return err
	}
	defer preparedStatement.Close()

	_, err = preparedStatement.Exec(c.Name, c.Address)
	if err != nil {
		return err
	}
	return nil
}