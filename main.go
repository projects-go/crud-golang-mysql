package main

import (
	"os"
	"bufio"
	"fmt"
)

func main() {
	menu := `¿Qué deseas hacer?
	[1] -- Insertar
	[2] -- Mostrar
	[3] -- Actualizar
	[4] -- Eliminar
	[5] -- Salir
	----->	`
	var eleccion int
	var c Contact
	for eleccion != 5 {
		fmt.Print(menu)
		fmt.Scanln(&eleccion)
		scanner := bufio.NewScanner(os.Stdin)
		switch eleccion {
			case 1:
			fmt.Println("Ingresa el nombre:")
			if scanner.Scan() {
				c.Name = scanner.Text()
			}
			fmt.Println("Ingresa la dirección:")
			if scanner.Scan() {
				c.Address = scanner.Text()
			}
			err := create(c)
			if err != nil {
				fmt.Printf("Error insertando: %v", err)
			} else {
				fmt.Println("Insertado correctamente")
			}
			case 2:
			contacts, err := read()
			if err != nil {
				fmt.Printf("Error obteniendo contactos: %v", err)
			} else {
				for _, contact := range contacts {
					fmt.Println("====================")
					fmt.Printf("Id: %d\n", contact.Id)
					fmt.Printf("Nombre: %s\n", contact.Name)
					fmt.Printf("Dirección: %s\n", contact.Address)
				}
			}
			case 3:
			fmt.Println("Ingresa el id:")
			fmt.Scanln(&c.Id)
			fmt.Println("Ingresa el nuevo nombre:")
			if scanner.Scan() {
				c.Name = scanner.Text()
			}
			fmt.Println("Ingresa la nueva dirección:")
			if scanner.Scan() {
				c.Address = scanner.Text()
			}
			err := update(c)
			if err != nil {
				fmt.Printf("Error actualizando: %v", err)
			} else {
				fmt.Println("Actualizado correctamente")
			}
			case 4:
			fmt.Println("Ingresa el ID del contacto que deseas eliminar:")
			fmt.Scanln(&c.Id)
			err := delete(c)
			if err != nil {
				fmt.Printf("Error eliminando: %v", err)
			} else {
				fmt.Println("Eliminado correctamente")
			}
			case 5:
				break
			default:
				fmt.Println("Valor incorrecto. Ingrese nuevamente")
		}
	}
}